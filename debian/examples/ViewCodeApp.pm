package ViewCodeApp;
use strict;
use warnings;
use base qw(CGI::Application);
use CGI::Application::Plugin::ViewCode;

sub setup {
    my $self = shift;
    $self->start_mode('start');
    $self->run_modes(start=>'start');
    return;
}

sub start {
    my $self = shift;
    return "Hello world!";
}

1;

=head1 DESCRIPTION

This CGI script is only a test example. It provides the following run
modes:

=over

=item start - Returns a simple "Hello world" string.

=item view_code - As provided by L<CGI::Application::Plugin::ViewCode>.

=item view_pod  - As provided by L<CGI::Application::Plugin::ViewCode>.
